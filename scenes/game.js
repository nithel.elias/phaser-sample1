import Phaser from "phaser";
var ball;
var speed = 200;
var paddle = {
  gameObject: null,
  keydown: false,
  ballOn: true,
};
export default class GameScene extends Phaser.Scene {
  constructor() {
    super("game");
  }

  create() {
    var bounceSound = this.sound.add("bounce");
    var breakSound = this.sound.add("break");
    this.add.image(this.scale.width / 2, 60, "logo");
    this.add.image(20, 20, "vite");
    this.add.image(60, 20, "javascript");

    ball = this.add.circle(
      this.scale.width / 2,
      this.scale.height - 80,
      12,
      0xff0000
    );

    this.physics.add.existing(ball);
    ball.body.setBounce(1, 1);
    ball.body.setCollideWorldBounds(true);
    //ball.body.setVelocity(speed, speed);

    let bricks = this.createBricks();
    this.bricks = bricks;
    this.physics.add.collider(ball, bricks, (_ball, _rect) => {
      _rect.destroy();
      breakSound.play();
    });

    let rect = this.add.rectangle(
      300,
      this.scale.height - 60,
      64,
      16,
      0xffffff
    );

    this.physics.add.existing(rect);
    rect.body.setImmovable(true);
    rect.body.setCollideWorldBounds(true);
    paddle.gameObject = rect;
    this.physics.add.collider(ball, rect, (_ball, _rect) => {
      let w = rect.width / 2;
      let difX = ball.x - rect.x;
      let difP = difX / w;
      ball.body.setVelocityX(300 * difP);
      bounceSound.play();
    });
    this.input.on("pointermove", (pointer) => {
      rect.x = pointer.x;
    });

    this.input.keyboard.on("keydown", (pointer) => {
      if (pointer.key === "ArrowLeft") {
        rect.body.setVelocity(-300, 0);
        paddle.keydown = true;
      }
      if (pointer.key === "ArrowRight") {
        rect.body.setVelocity(300, 0);
        paddle.keydown = true;
      }
    });
    this.input.keyboard.on("keyup", (pointer) => {
      paddle.keydown = false;
    });
    this.input.on("pointerdown", (pointer) => {
      paddle.ballOn = false;
      ball.body.setVelocity(speed, speed);
    });
  }

  createBricks() {
    const scene = this;
    const bricks = scene.physics.add.staticGroup();
    const totalBricks = 48;
    var x = 100,
      y = 100;
    const width = 48,
      height = 16,
      color = 0x0affa0;

    for (let i = 1; i <= totalBricks; i++) {
      var rect = scene.add.rectangle(x, y, width, height, color);
      bricks.add(rect);
      x += width + 8;
      if (i % 12 == 0) {
        y += height + 8;
        x = 100;
      }
    }

    return bricks;
  }
  update(time, delta) {
    if (!paddle.keydown) {
      paddle.gameObject.body.setVelocity(
        paddle.gameObject.body.velocity.x * 0.86,
        0
      );
    }
    if (paddle.ballOn) {
      ball.setPosition(paddle.gameObject.x, paddle.gameObject.y - 20);
    }

    if(this.bricks.getChildren().length===0){
      this.end(true);
      return
    }

    if(ball.y>paddle.gameObject.y+paddle.gameObject.height){
      this.end(false);
      return
    }
  }
  end(win) {
    this.scene.pause();
    this.scene.run("end", { win });
  }
}
