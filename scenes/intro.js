import Phaser from "phaser";
export default class IntroScene extends Phaser.Scene {
  constructor() {
    super("intro");
  }
  preload() {
    this.load.image("logo", "logo.png");
    this.load.image("vite", "vite.svg");
    this.load.image("javascript", "javascript.svg");
    this.load.audio("bounce", "bounce.wav");
    this.load.audio("break", "break.wav");
  }
  create() {
    this.add.image(this.scale.width / 2, 300, "logo");

    let text = this.add
      .text(this.scale.width / 2, 500, "Click to start", {
        fontSize: "32px",
      })
      .setOrigin(0.5).setAlpha(1)

    this.input.on("pointerdown", () => {
      this.goToGame();
    });

    this.add.tween({
      targets: text,
      alpha: 0,
      yoyo: true,
      repeat: -1,
      duration: 300,
      ease: "Power1",
    });
  }
  goToGame() {
    this.scene.stop();
    this.scene.start("game");
  }
}
