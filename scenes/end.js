export default class EndScene extends Phaser.Scene {
  constructor() {
    super("end");
  }
  create({ win = false }) {
    this.add
      .text(
        this.scale.width / 2,
        this.scale.height / 2,
        win ? "You win!" : "You Lose"
      )
      .setOrigin(0.5);
  }
}
