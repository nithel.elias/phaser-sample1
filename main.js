import Phaser, { Game } from "phaser";
import "./style.css";
import GameScene from "./scenes/game";
import IntroScene from "./scenes/intro";
import EndScene from "./scenes/end";
new Game({
  width: 800,
  height: 600,
  type: Phaser.AUTO,
  parent: "app",
  scene: [IntroScene, GameScene, EndScene],
  fps: {
    target: 60,
    forceSetTimeOut: true,
  },
  physics: {
    default: "arcade",
  },
});
